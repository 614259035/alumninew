-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 29, 2020 at 03:54 PM
-- Server version: 8.0.17
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `register`
--

-- --------------------------------------------------------

--
-- Table structure for table `add2`
--

CREATE TABLE `add2` (
  `c_id` int(11) NOT NULL,
  `home2` varchar(50) NOT NULL,
  `mo2` varchar(10) NOT NULL,
  `song2` varchar(55) NOT NULL,
  `road2` varchar(55) NOT NULL,
  `tampon2` varchar(555) NOT NULL,
  `ampor2` varchar(555) NOT NULL,
  `provice2` varchar(555) NOT NULL,
  `zip` int(11) NOT NULL,
  `phone_h` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `add2`
--

INSERT INTO `add2` (`c_id`, `home2`, `mo2`, `song2`, `road2`, `tampon2`, `ampor2`, `provice2`, `zip`, `phone_h`) VALUES
(3, '232', '4', '-', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', 70130, '0803024948'),
(4, '56456', '65456', '56456', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', 56456456, '65645465465'),
(5, '45645', '645', '6456', '645', '6456', '456', '45654', 456, '456456456'),
(6, '232', '4', '-', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', 70130, '0803024948'),
(7, '322', '4', '6456', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', 70130, '0803024948'),
(8, '232', '4', '-', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', 1244421, '09154215431');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `c_id` int(11) NOT NULL,
  `homen` varchar(555) NOT NULL,
  `mo` varchar(10) NOT NULL,
  `song` varchar(500) NOT NULL,
  `road` varchar(500) NOT NULL,
  `tampon` varchar(500) NOT NULL,
  `ampor` varchar(500) NOT NULL,
  `provice` varchar(500) NOT NULL,
  `codezip` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`c_id`, `homen`, `mo`, `song`, `road`, `tampon`, `ampor`, `provice`, `codezip`) VALUES
(3, '232', '4', '-', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', '70130'),
(4, '232', '6', '-', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', '56456456'),
(5, '456', '456456', '6456', '645645', '45645', '645645', '6456', '45'),
(6, '232', '4', '4545', '-', '45645', 'ดำเนิน', 'ราชบุรี', '701230'),
(7, '232', '4', '4545', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', '70130'),
(8, '232', '4', '-', '-', 'แพงพวย', 'ดำเนิน', 'ราชบุรี', '12444421');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `c_id` int(11) NOT NULL,
  `s_id` varchar(10) NOT NULL,
  `section` varchar(10) NOT NULL,
  `grade` varchar(6) NOT NULL,
  `regis_year` varchar(10) NOT NULL,
  `sus_year` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`c_id`, `s_id`, `section`, `grade`, `regis_year`, `sus_year`) VALUES
(3, '614259007', '61/47', '4', '2560', '2563'),
(4, '614259005', '64', '3', '2560', '5656'),
(5, '614259004', '45645', '456', '45645', '45645'),
(6, '614259003', '61/47', '3', '2555', '2555'),
(7, '614259001', '61/47', '3', '2556', '2556'),
(8, '614259009', '61/47', '4.00', '2566', '2570');

-- --------------------------------------------------------

--
-- Table structure for table `name`
--

CREATE TABLE `name` (
  `c_id` int(11) NOT NULL,
  `mr` varchar(50) NOT NULL,
  `fname` varchar(500) NOT NULL,
  `mre` varchar(50) NOT NULL,
  `ename` varchar(50) NOT NULL,
  `id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `job` varchar(50) NOT NULL,
  `nation` varchar(50) NOT NULL,
  `nation2` varchar(50) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `blood` varchar(3) NOT NULL,
  `email` varchar(555) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `facebook` varchar(555) NOT NULL,
  `line` varchar(555) NOT NULL,
  `rey` varchar(10) NOT NULL,
  `psw` varchar(10) NOT NULL,
  `s_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `name`
--

INSERT INTO `name` (`c_id`, `mr`, `fname`, `mre`, `ename`, `id`, `date`, `job`, `nation`, `nation2`, `religion`, `blood`, `email`, `tel`, `facebook`, `line`, `rey`, `psw`, `s_id`) VALUES
(3, 'นาย', '  เฉลิมรัฐ พรมสอาด', 'Mr.', '  chaleomrat phromsaart', '  1739901838730  ', '2020-10-04', '  คนหล่อ', '  ไทย', 'ไทย', 'พุทธ', 'x', 'fxcz.777@gmail.com', '0910044851', 'chaleomrat phromsaart', '0910044851', '2560', '01234566+', '614259007'),
(4, 'นาย', ' emi Fukada', 'Mr.', ' emi Fukada', ' 5489789565 ', '2020-10-01', ' คน', ' ไทย', 'ไทย', 'พุทธ', 'o', '614259007@webmail.npru.ac.th', '65456', 'emi', '0910044851', '2560', '01234566+', '614259005'),
(5, 'นาย', 'xxxxx', 'Mr.', 'xxxxxxxxxxxxxxx', '6456', '2020-10-04', '456456', 'ไทย', 'ไทย', 'พุทธ', 'o', 'fxcz.777@gmail.com', '456456', 'chaleomrat phromsaart', '0910044851', '45645', '6456456', '614259004'),
(6, 'นาย', 'ธนาพล ใจตรง', 'Mr.', 'thanapon jaitong', '173990183873200', '2020-10-16', '456456', 'ไทย', 'ไทย', 'พุทธ', 'o', 'txcz.777@gmail.com', '0803024948', 'thanapon jaitong', '0910044851', '2555', '01234566', '614259003'),
(7, 'นาย', ' ธนากิต', 'Mr.', ' thanakit tinop', ' 173990183745 ', '2020-10-16', ' คน', ' ไทย', 'ไทย', 'พุทธ', 'o', '614259030@webmail.npru.ac.th', '0803024948', 'thanapon jaitong', '0910044851', '2556', '01234566', '614259001'),
(8, 'นางสาว', 'คาโอริ', 'Miss.', 'kaori', '173990818345', '2020-10-29', 'นักแสดง', 'ญี่ปุ่น', 'ญี่ปุ่น', 'พุทธ', 'o', 'kaorii@gmail.com', '0803024984', 'kaorichan', 'k0015', '2566', '01234566+', '614259009');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add2`
--
ALTER TABLE `add2`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `name`
--
ALTER TABLE `name`
  ADD PRIMARY KEY (`c_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add2`
--
ALTER TABLE `add2`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `name`
--
ALTER TABLE `name`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `add2`
--
ALTER TABLE `add2`
  ADD CONSTRAINT `add2_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `name` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `name` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `name` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

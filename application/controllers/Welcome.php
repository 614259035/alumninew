<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_reg', 'Alumni');
        $this->load->library('session', 'database');
    }
    public function index()
    {
        $this->load->view('bootstrap');
        $this->load->view('Navbar');
        $this->load->view('home_view');
    }

    public function reg()
    {

        $data = array(
            'mr' => $this->input->post("mr"),
            'fname' => $this->input->post("fname"),
            'mre' => $this->input->post("mre"),
            'ename' => $this->input->post("ename"),
            'id' => $this->input->post("id"),
            'date' => $this->input->post("date"),
            'job' => $this->input->post("job"),
            'nation' => $this->input->post("nation"),
            'nation2' => $this->input->post("nation2"),
            'religion' => $this->input->post("re"),
            'blood' => $this->input->post("blood"),
            'email' => $this->input->post("email"),
            'tel' => $this->input->post("tel"),
            'facebook' => $this->input->post("face"),
            'line' => $this->input->post("line"),
            'rey' => $this->input->post("ry"),
            'psw' => $this->input->post("pass"),
            's_id' => $this->input->post("sid")


        );

        $data2 = array(
            'homen' => $this->input->post("homea"),
            'mo' => $this->input->post("mou"),
            'song' => $this->input->post("song"),
            'road' => $this->input->post("road"),
            'tampon' => $this->input->post("tampon"),
            'ampor' => $this->input->post("ampor"),
            'provice' => $this->input->post("prov"),
            'codezip' => $this->input->post("zipcode")


        );
        $data3 = array(
            'home2' => $this->input->post("h2"),
            'mo2' => $this->input->post("m2"),
            'song2' => $this->input->post("s2"),
            'road2' => $this->input->post("road2"),
            'tampon2' => $this->input->post("t2"),
            'ampor2' => $this->input->post("a2"),
            'provice2' => $this->input->post("prov2"),
            'zip' => $this->input->post("zip2"),
            'phone_h' => $this->input->post("phone")

        );
        $data4 = array(
            's_id' => $this->input->post("sid"),
            'section' => $this->input->post("sec"),
            'grade' => $this->input->post("grade"),
            'regis_year' => $this->input->post("ry"),
            'sus_year' => $this->input->post("sy")

        );
        $this->Alumni->Alumni_name($data);
        $this->Alumni->Alumni_ad1($data2);
        $this->Alumni->Alumni_ad2($data3);
        $this->Alumni->Alumni_history($data4);


        $this->load->view('bootstrap');
        $this->load->view('Navbar');
        $this->load->view('home_view');
    }

    public function showall()
    {

        $data['query'] = $this->Alumni->show_all();
        $this->load->view('view_main', $data);
    }

    public function search()
    {
        $search =   $this->input->post('search');
        $data['xx'] = $this->Alumni->show_search($search);
        $this->load->view('view_search', $data);
    }

    public function edit_show()
    {
        $id =  $this->session->userdata('c_id');
        $data['re'] = $this->Alumni->select_name($id);
        $this->load->view('edite_data', $data);
    }

    public function viewinfo()
    {
        $id =  $this->session->userdata('c_id');
        $data['re'] = $this->Alumni->select_name($id);
        $this->load->view('viewinfo', $data);
    }

    public function update()
    {

        $data = array(
            'fname' => $this->input->post("fname"),
            'ename' => $this->input->post("ename"),
            'id' => $this->input->post("id"),
            'job' => $this->input->post("job"),
            'nation' => $this->input->post("nation"),
            'nation2' => $this->input->post("nation2"),
            'religion' => $this->input->post("re"),
            'blood' => $this->input->post("blood"),
            'email' => $this->input->post("email"),
            'tel' => $this->input->post("tel"),
            'facebook' => $this->input->post("face"),
            'line' => $this->input->post("line"),
            'rey' => $this->input->post("ry")
        );

        $data2 = array(
            'homen' => $this->input->post("homea"),
            'mo' => $this->input->post("mou"),
            'song' => $this->input->post("song"),
            'road' => $this->input->post("road"),
            'tampon' => $this->input->post("tampon"),
            'ampor' => $this->input->post("ampor"),
            'provice' => $this->input->post("prov"),
            'codezip' => $this->input->post("zipcode")


        );

        $data3 = array(
            'home2' => $this->input->post("h2"),
            'mo2' => $this->input->post("m2"),
            'song2' => $this->input->post("s2"),
            'road2' => $this->input->post("road2"),
            'tampon2' => $this->input->post("t2"),
            'ampor2' => $this->input->post("a2"),
            'provice2' => $this->input->post("prov2"),
            'zip' => $this->input->post("zip2"),
            'phone_h' => $this->input->post("phone")

        );

        $data4 = array(
            's_id' => $this->input->post("sid"),
            'section' => $this->input->post("sec"),
            'grade' => $this->input->post("grade"),
            'regis_year' => $this->input->post("ry"),
            'sus_year' => $this->input->post("sy")

        );

        $this->Alumni->update_name($data);
        $this->Alumni->update_address($data2);
        $this->Alumni->update_add2($data3);
        $this->Alumni->update_his($data4);
        $id =  $this->session->userdata('c_id');
        $dataa['re'] = $this->Alumni->select_name($id);
        $this->load->view('viewinfo', $dataa);
    }

    public function view_other()
    {
        $id =  $this->input->post('c_id');
        $data['re'] = $this->Alumni->select_other($id);
        $this->load->view('view_other', $data);
    }
    public function indexlog()
    {
        $this->load->library('session');
        $this->load->model('Mod_reg', 'Alumni');
        $this->load->view('bootstrap');
        $this->load->view('Navbar');
        $this->load->view('login');
    }

    public function login_x()

    {

        if ($this->input->post('login')) {
            $s_id = $this->input->post('s_id');
            $password = $this->input->post('psw');
            $check = $this->Alumni->check_login($s_id, $password);

            if ($check['message'] == true) {
                $array = json_decode(json_encode($check['data']), true);
                $this->session->set_userdata($array[0]);
                $this->load->view('bootstrap');
                $this->load->view('Navbar');
                $this->load->view('home_view');
            } else {



                // echo '<script>alert("Region already added");</script>'
                redirect('Welcome/indexlog');
            }
        }
    }
    public function logout()
    {

        $this->session->sess_destroy();
        $this->session->unset_userdata($data);

        $this->load->view('bootstrap');
        redirect($this->load->view('Navbar'));
        redirect('index');
    }

    public function indexRegister()
    {
        $this->load->view('bootstrap');
        $this->load->view('Navbar');
        $this->load->view('register_view');
    }
    public function reg_menber()
    {
        $this->load->view('bootstrap');
        $this->load->view('Navbar');
        $this->load->view('register_menber');
    }
    public function indexview()
    {
        $this->load->view('bootstrap');
        $this->load->view('Navbar');
        $this->load->view('view_main');
    }
}

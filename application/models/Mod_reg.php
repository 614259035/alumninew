<?php defined('BASEPATH') or exit('No direct script access allowed');
class Mod_reg extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function show_all()
    {
        $query = $this->db->get('name');
        return $query->result();
    }

    function Alumni_name($data)
    {
        $this->db->insert('name', $data);
    }

    function Alumni_ad1($data)
    {
        $this->db->insert('address', $data);
    }
    function Alumni_ad2($data)
    {
        $this->db->insert('add2', $data);
    }
    function Alumni_history($data)
    {
        $this->db->insert('history', $data);
    }

    function check_login($s_id, $password)
    {
        // $this->db->select('email,password');
        $this->db->where('s_id', $s_id);
        $this->db->where('psw', $password);
        $result = $this->db->get('name');
        $message = '';

        if ($result->num_rows() > 0) {
            $message = true;

            // $this->load->view('list');

        } else {
            $message = false;
        }
        $data = array(
            "message" => $message, "data" => $result->result()

        );
        return $data;
    }


    function show_search($search)
    {
        $this->db->like('name' . "." . 'fname', $search);
        $this->db->or_like('history' . "." . 's_id', $search);
        $this->db->or_like('history' . "." . 'section', $search);
        $this->db->or_like('history' . "." . 'regis_year', $search);
        $this->db->or_like('address' . "." . 'provice', $search);
        $this->db->join('history', 'history.c_id = name.c_id');
        $this->db->join('address', 'address.c_id = name.c_id');
        $result = $this->db->get('name');

        return $result->result();
    }



    function select_name($id)
    {

        $this->db->select('*');
        $this->db->from('name');
        $this->db->join('address', 'address.c_id = name.c_id', 'left');
        $this->db->join('add2', 'add2.c_id = name.c_id', 'left');
        $this->db->join('history', 'history.c_id = name.c_id', 'left');
        $this->db->where('name.c_id', $id);

        $query = $this->db->get();
        return $query->result();
    }

    function update_name($data)
    {
        $id =  $this->session->userdata('c_id');
        $this->db->where('c_id', $id);
        $this->db->update('name', $data);
    }
    function update_address($data)
    {
        $id =  $this->session->userdata('c_id');
        $this->db->where('c_id', $id);
        $this->db->update('address', $data);
    }
    function update_add2($data)
    {
        $id =  $this->session->userdata('c_id');
        $this->db->where('c_id', $id);
        $this->db->update('add2', $data);
    }

    function update_his($data)
    {
        $id =  $this->session->userdata('c_id');
        $this->db->where('c_id', $id);
        $this->db->update('history', $data);
    }

    function select_other($id)
    {

        $this->db->select('*');
        $this->db->from('name');
        $this->db->join('history', 'history.c_id = name.c_id', 'left');
        $this->db->where('name.c_id', $id);
        $query = $this->db->get();
        return $query->result();
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstrap');
$this->load->view('Navbar');
?>
<div class="container">
    <div class="card card-out">
        <div class="card-body card-body-in">
            <h4 style="margin: 0px">ข้อมูลศิษย์เก่าทั้งหมด</h4>
            <div class="dropdown-divider"></div>
            <div align="center">
                <div style="max-width: 500px">
                    <p style="max-width: 400px; font-size: 18px" align="center">
                        <form method="POST" action="">
                            <div class="row">
                                <form method="post" action="<?php echo site_url('Welcome/search'); ?>">
                                    <div class="row">
                                        <div class="col-9">
                                            <input class="form-control" type="text" placeholder="ค้นหาโดยใช้ ชื่อ-นามสกุล" name="search" />
                                        </div>
                                        <div class="col-3">
                                            <input type="submit" class="btn btn-block btn-primary" style="width:100px" name="submit" value="ค้นหา">
                                        </div>
                                </form>
                            </div>
                    </p>
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">รหัส</th>
                        <th scope="col">ชื่อ-นามสกุล</th>
                        <th scope="col">ปีการศึกษา</th>
                        <th scope="col">รายระเอียด</th>

                        <!-- <th scope="col">ลบ</th> -->

                    </tr>
                </thead>
                <?php
                if ($xx != null) {
                    foreach ($xx as $x) {

                ?>

                        <tbody>
                            <tr>
                                <th scope="row"> <?php echo $x->c_id; ?> </th>
                                <th scope="row"> <?php echo $x->fname; ?> </th>
                                <th scope="row"> <?php echo $x->rey; ?> </th>
                                <td>
                                    <form action="./view_other" method="POST">
                                        <input type="text" name="c_id" value="<?php echo $x->c_id; ?>" hidden>
                                        <input type="submit" class="btn btn-success" name="submit" value="ดูรายละเอียด"></form></a>
                                </td>

                            </tr>
                        </tbody>
                    <?php
                    }
                } else {
                    ?>
                    <h1 style="color:red;font-size:30px;margin:30px;padding:0;padding-top:10px">ไม่พบชื่อคนนี้</h1>
                <?php
                }

                ?>
            </table>

        </div>
    </div>
</div>
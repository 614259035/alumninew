<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstrap');
$this->load->view('Navbar');
?>
<div class="container">
    <div class="card card-out">
        <div class="card-body card-body-in">
            <h4 style="margin: 0px">แก้ไขรายละเอียดของศิษย์เก่า</h4>
            <div class="dropdown-divider"></div>
            <h5>ข้อมูลส่วนตัวของศิษย์เก่า</h5>
            <div align="center">
                <form method="POST" action="update">
                    <?php foreach ($re as $n) {
                    ?>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-3" style="margin-bottom: 0">คำนำหน้า
                                <input class="form-control" type="text" placeholder="คำนำหน้า" maxlength="" name="" value=" <?php echo $n->mr; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-9" style="margin-bottom: 0">ชื่อ-นามสกุล (ภาษาไทย)
                                <input class="form-control" type="text" placeholder="ชื่อ-นามสกุล (ภาษาไทย)" maxlength="" name="fname" value=" <?php echo $n->fname; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-3" style="margin-bottom: 0">คำนำหน้า
                                <input class="form-control" type="text" placeholder="คำนำหน้า" maxlength="" name="" value=" <?php echo $n->mre; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-9" style="margin-bottom: 0">ชื่อ-นามสกุล (ภาษาอังกฤษ)
                                <input class="form-control" type="text" placeholder="ชื่อ-นามสกุล (ภาษาอังกฤษ)" maxlength="" name="ename" value=" <?php echo $n->ename; ?>" />
                            </div>
                        </div>
                        <p style="max-width: 600px; font-size: 18px" align="left">หมายเลขบัตรประจำตัวประชาชน 13 หลัก
                            <input class="form-control" type="text" placeholder="หมายเลขบัตรประจำตัวประชาชน" maxlength="" name="id" value=" <?php echo $n->id; ?> " />
                        </p>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">วันเกิด
                                <input class="form-control" type="text" placeholder="วันเกิด" maxlength="" name="" value=" <?php echo $n->date; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">ตำแหน่งงาน
                                <input class="form-control" type="text" placeholder="ตำแหน่งงาน" maxlength="" name="job" value=" <?php echo $n->job; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-3" style="margin-bottom: 0">เชื้อชาติ
                                <input class="form-control" type="text" placeholder="เชื้อชาติ" maxlength="" name="nation" value=" <?php echo $n->nation; ?>" />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">สัญชาติ
                                <input class="form-control" type="text" placeholder="สัญชาติ" maxlength="" name="nation2" value="<?php echo $n->nation2; ?>" />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">ศาสนา
                                <input class="form-control" type="text" placeholder="ศาสนา" maxlength="" name="re" value="<?php echo $n->religion; ?>" />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">กรุ๊ปเลือด
                                <input class="form-control" type="text" placeholder="กรุ๊ปเลือด" maxlength="" name="blood" value="<?php echo $n->blood; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">อีเมล
                                <input class="form-control" type="email" placeholder="อีเมล" maxlength="" name="email" value="<?php echo $n->email; ?>" />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">หมายเลขโทรศัพท์
                                <input class="form-control" type="number" placeholder="หมายเลขโทรศัพท์" maxlength="" name="tel" value="<?php echo $n->tel; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">Facebook
                                <input class="form-control" type="text" placeholder="Facebook" maxlength="" name="face" value="<?php echo $n->facebook; ?>" />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">Line ID
                                <input class="form-control" type="text" placeholder="Line ID" maxlength="" name="line" value="<?php echo $n->line; ?>" />
                            </div>
                        </div>

                        <div class="dropdown-divider"></div>
                        <h5 align="left">ที่อยู่ที่สามารถติดต่อได้</h5>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">บ้านเลขที่
                                <input class="form-control" type="number" placeholder="บ้านเลขที่" maxlength="" name="homea" value="<?php echo $n->homen; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">หมู่
                                <input class="form-control" type="number" placeholder="หมู่" maxlength="" name="mou" value="<?php echo $n->mo; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ซอย
                                <input class="form-control" type="text" placeholder="ซอย" maxlength="" name="song" value="<?php echo $n->song; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ถนน
                                <input class="form-control" type="text" placeholder="ถนน" maxlength="" name="road" value="<?php echo $n->road; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ตำบล/แขวง
                                <input class="form-control" type="text" placeholder="ตำบล/แขวง" maxlength="" name="tampon" value="<?php echo $n->tampon; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">อำเภอ/เขต
                                <input class="form-control" type="text" placeholder="อำเภอ/เขต" maxlength="" name="ampor" value="<?php echo $n->ampor; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">จังหวัด
                                <input class="form-control" type="text" placeholder="จังหวัด" maxlength="" name="prov" value="<?php echo $n->provice; ?>" />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">รหัสไปรษณีย์
                                <input class="form-control" type="number" placeholder="รหัสไปรษณีย์" maxlength="" name="zipcode" value="<?php echo $n->codezip; ?>" />
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <h5 align="left">ที่อยู่ที่ทำงาน</h5>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">บ้านเลขที่
                                <input class="form-control" type="number" placeholder="บ้านเลขที่" maxlength="" name="h2" value="<?php echo $n->home2; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">หมู่
                                <input class="form-control" type="number" placeholder="หมู่" maxlength="" name="m2" value="<?php echo $n->mo2; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ซอย
                                <input class="form-control" type="text" placeholder="ซอย" maxlength="" name="s2" value="<?php echo $n->song2; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ถนน
                                <input class="form-control" type="text" placeholder="ถนน" maxlength="" name="road2" value="<?php echo $n->road2; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ตำบล/แขวง
                                <input class="form-control" type="text" placeholder="ตำบล/แขวง" maxlength="" name="t2" value="<?php echo $n->tampon2; ?>" />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">อำเภอ/เขต
                                <input class="form-control" type="text" placeholder="อำเภอ/เขต" maxlength="" name="a2" value="<?php echo $n->ampor2; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">จังหวัด
                                <input class="form-control" type="text" placeholder="จังหวัด" maxlength="" name="prov2" value="<?php echo $n->provice2; ?>" />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">รหัสไปรษณีย์
                                <input class="form-control" type="number" placeholder="รหัสไปรษณีย์" maxlength="" name="zip2" value="<?php echo $n->zip; ?>" />
                            </div>
                        </div>
                        <p style="max-width: 600px; font-size: 18px" align="left">หมายเลขโทรศัพท์ที่ทำงาน
                            <input class="form-control" type="number" placeholder="หมายเลขโทรศัพท์ที่ทำงาน" maxlength="" name="phone" value="<?php echo $n->phone_h; ?>" />
                        </p>

                        <div class="dropdown-divider"></div>
                        <h5 align="left">ประวัติการศึกษา</h5>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">รหัสนักศึกษา
                                <input class="form-control" type="number" placeholder="รหัสนักศึกษา" maxlength="" name="sid" value="<?php echo $n->s_id; ?>" />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">หมู่เรียน
                                <input class="form-control" type="text" placeholder="หมู่เรียน" maxlength="" name="sec" value="<?php echo $n->section; ?>" />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">เกรดเฉลี่ย
                                <input class="form-control" type="number" placeholder="เกรดเฉลี่ย" maxlength="" name="grade" value="<?php echo $n->grade; ?>" />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">ปีการศึกษาที่เข้าศึกษา
                                <input class="form-control" type="number" placeholder="ปีการศึกษาที่เข้าศึกษา" maxlength="" name="ry" value="<?php echo $n->regis_year; ?>" />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">ปีการศึกษาที่สำเร็จการศึกษา
                                <input class="form-control" type="number" placeholder="ปีการศึกษาที่สำเร็จการศึกษา" maxlength="" name="sy" value="<?php echo $n->sus_year; ?>" />
                            </div>
                        </div>
                    <?php } ?>
                    <div class="dropdown-divider"></div>
                    <button type="submit" class="btn btn-primary">บันทึก</button>

                    <a href="<?php echo site_url('Welcome'); ?>"><button type="button" class="btn btn-danger">ปิด</button></a>
                </form>
            </div>

        </div>
    </div>
</div>
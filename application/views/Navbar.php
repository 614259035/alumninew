<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <div class="container">
        <a class="navbar-brand" >
            <img src="<?php echo base_url('img'); ?>/logo.png" height="90" class="d-inline-block align-top" alt="">
            <a style="margin-top: 10px"><strong>Software Engineering Nakhon Pathom Rajabhat University</strong></a>
        </a>
    </div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-bottom: 10px; background-color: #33B5E5;">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url('Welcome'); ?>" style="color:white">หน้าหลัก <span class="sr-only">(current)</span></a>
                </li>

                <?php if (isset($this->session->userdata['fname'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('Welcome/edit_show'); ?>" style="color:white">แก้ไขข้อมูลผู้ใช้</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('Welcome/viewinfo'); ?>" style="color:white">ดูข้อมูลผู้ใช้</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('Welcome/showall'); ?>" style="color:white">ข้อมูลศิษย์เก่าทั้งหมด</a>
                    </li> <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('Welcome/indexRegister'); ?>" style="color:white">ลงทะเบียน</a>
                    </li>
                
                <?php } ?>
            </ul>
        </div>
        <?php if (isset($this->session->userdata['fname'])) { ?>
            <h4 style="color:white;font-size:18px;margin-right:20px;padding:0;padding-top:10px"><?php echo $this->session->userdata('title') . ' ' . $this->session->userdata('fname') . ' ' . $this->session->userdata('lname'); ?></h4>

            <form class="form-inline">
                <a class="nav-link" href="<?php echo site_url('Welcome/logout'); ?>" style="color:white;margin-right:10px;padding:0;padding-top:18px"><button class="btn btn-outline-light my-2 my-sm-0" type="button">ออกจากระบบ</button></a>
            </form>
        <?php } else { ?>
            <form class="form-inline">
                <a class="nav-link" href="<?php echo site_url('Welcome/indexlog'); ?>" style="color:white;margin-right:10px;padding:0;padding-top:18px"><button class="btn btn-outline-light my-2 my-sm-0" type="button">เข้าสู่ระบบ</button></a>
            </form>
           
        <?php } ?>
    </div>
</nav>
<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstrap');
$this->load->view('Navbar');
?>
<div class="container">
    <div class="card card-out">
        <div class="card-body card-body-in">
            <h4 style="margin: 0px">ข้อมูลศิษย์เก่าทั้งหมด</h4>
            <div class="dropdown-divider"></div>
            <div align="center">
                <div style="max-width: 500px">
                    <p style="max-width: 400px; font-size: 18px" align="center">
                        <form method="post" action="search">
                            <div class="row">
                                <div class="col-9">
                                    <input class="form-control" type="text" placeholder="ค้นหาโดยใช้ รหัสนักศึกษา, หมู่เรียน, ปีการศึกษาแรกเข้า, ชื่อ-นามสกุล, จังหวัดที่อยู่" name="search" />
                                </div>
                                <div class="col-3">
                                    <input type="submit" class="btn btn-block btn-primary" name="submit" value="ค้นหา">
                                </div>
                        </form>
                </div>
                </p>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">รหัส</th>
                    <th scope="col">ชื่อ-นามสกุล</th>
                    <th scope="col">ปีการศึกษา</th>
                    <th scope="col">รายละเอียด</th>

                    <!-- <th scope="col">ลบ</th> -->

                </tr>
            </thead>
            <?php
            foreach ($query as $x) {

            ?><tbody>
                    <tr>
                        <th scope="row"> <?php echo $x->c_id; ?> </th>
                        <th scope="row"> <?php echo $x->fname; ?> </th>
                        <th scope="row"> <?php echo $x->rey; ?> </th>
                        <td>
                            <form action="./view_other" method="POST">
                                <input type="text" name="c_id" value="<?php echo $x->c_id; ?>" hidden>
                                <input type="submit" class="btn btn-success" name="submit" value="ดูรายละเอียด"></form></a>
                        </td>
                        <!-- <td><a href=""><button type="button" class="btn btn-danger">ลบ</button></a></td> -->

                    </tr>
                </tbody>
            <?php } ?>
        </table>

    </div>
</div>
</div>
<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstrap');
$this->load->view('Navbar');
?>
<div class="container">
    <div class="card card-out">
        <div class="card-body card-body-in">
            <h4 style="margin: 0px">ดูรายละเอียดของศิษย์เก่า</h4>
            <div class="dropdown-divider"></div>
            <h5>ข้อมูลส่วนตัวของศิษย์เก่า</h5>
            <div align="center">
                <form method="" action="">
                    <?php foreach ($re as $n) {
                    ?>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-3" style="margin-bottom: 0">คำนำหน้า
                                <input class="form-control" type="text" placeholder="คำนำหน้า" maxlength="" name="" value=" <?php echo $n->mr; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-9" style="margin-bottom: 0">ชื่อ-นามสกุล (ภาษาไทย)
                                <input class="form-control" type="text" placeholder="ชื่อ-นามสกุล (ภาษาไทย)" maxlength="" name="" value=" <?php echo $n->fname; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-3" style="margin-bottom: 0">คำนำหน้า
                                <input class="form-control" type="text" placeholder="คำนำหน้า" maxlength="" name="" value=" <?php echo $n->mre; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-9" style="margin-bottom: 0">ชื่อ-นามสกุล (ภาษาอังกฤษ)
                                <input class="form-control" type="text" placeholder="ชื่อ-นามสกุล (ภาษาอังกฤษ)" maxlength="" name="" value=" <?php echo $n->ename; ?>" readonly />
                            </div>
                        </div>
                        <p style="max-width: 600px; font-size: 18px" align="left">หมายเลขบัตรประจำตัวประชาชน 13 หลัก
                            <input class="form-control" type="text" placeholder="หมายเลขบัตรประจำตัวประชาชน" maxlength="" name="" value=" <?php echo $n->id; ?>" readonly />
                        </p>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">วันเกิด
                                <input class="form-control" type="text" placeholder="วันเกิด" maxlength="" name="" value=" <?php echo $n->date; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">ตำแหน่งงาน
                                <input class="form-control" type="text" placeholder="ตำแหน่งงาน" maxlength="" name="" value=" <?php echo $n->job; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-3" style="margin-bottom: 0">เชื้อชาติ
                                <input class="form-control" type="text" placeholder="เชื้อชาติ" maxlength="" name="" value=" <?php echo $n->nation; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">สัญชาติ
                                <input class="form-control" type="text" placeholder="สัญชาติ" maxlength="" name="" value="<?php echo $n->nation2; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">ศาสนา
                                <input class="form-control" type="text" placeholder="ศาสนา" maxlength="" name="" value="<?php echo $n->religion; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">กรุ๊ปเลือด
                                <input class="form-control" type="text" placeholder="กรุ๊ปเลือด" maxlength="" name="" value="<?php echo $n->blood; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">อีเมล
                                <input class="form-control" type="email" placeholder="อีเมล" maxlength="" name="" value="<?php echo $n->email; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">หมายเลขโทรศัพท์
                                <input class="form-control" type="number" placeholder="หมายเลขโทรศัพท์" maxlength="" name="" value="<?php echo $n->tel; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">Facebook
                                <input class="form-control" type="text" placeholder="Facebook" maxlength="" name="" value="<?php echo $n->facebook; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">Line ID
                                <input class="form-control" type="text" placeholder="Line ID" maxlength="" name="" value="<?php echo $n->line; ?>" readonly />
                            </div>
                        </div>

                        <div class="dropdown-divider"></div>
                        <h5 align="left">ที่อยู่ที่สามารถติดต่อได้</h5>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">บ้านเลขที่
                                <input class="form-control" type="number" placeholder="บ้านเลขที่" maxlength="" name="" value="<?php echo $n->homen; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">หมู่
                                <input class="form-control" type="number" placeholder="หมู่" maxlength="" name="" value="<?php echo $n->mo; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ซอย
                                <input class="form-control" type="text" placeholder="ซอย" maxlength="" name="" value="<?php echo $n->song; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ถนน
                                <input class="form-control" type="text" placeholder="ถนน" maxlength="" name="" value="<?php echo $n->road; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ตำบล/แขวง
                                <input class="form-control" type="text" placeholder="ตำบล/แขวง" maxlength="" name="" value="<?php echo $n->tampon; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">อำเภอ/เขต
                                <input class="form-control" type="text" placeholder="อำเภอ/เขต" maxlength="" name="" value="<?php echo $n->ampor; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">จังหวัด
                                <input class="form-control" type="text" placeholder="จังหวัด" maxlength="" name="" value="<?php echo $n->provice; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">รหัสไปรษณีย์
                                <input class="form-control" type="number" placeholder="รหัสไปรษณีย์" maxlength="" name="" value="<?php echo $n->codezip; ?>" readonly />
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <h5 align="left">ที่อยู่ที่ทำงาน</h5>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">บ้านเลขที่
                                <input class="form-control" type="number" placeholder="บ้านเลขที่" maxlength="" name="" value="<?php echo $n->home2; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">หมู่
                                <input class="form-control" type="number" placeholder="หมู่" maxlength="" name="" value="<?php echo $n->mo2; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ซอย
                                <input class="form-control" type="text" placeholder="ซอย" maxlength="" name="" value="<?php echo $n->song2; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ถนน
                                <input class="form-control" type="text" placeholder="ถนน" maxlength="" name="" value="<?php echo $n->road2; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">ตำบล/แขวง
                                <input class="form-control" type="text" placeholder="ตำบล/แขวง" maxlength="" name="" value="<?php echo $n->tampon2; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-4" style="margin-bottom: 0">อำเภอ/เขต
                                <input class="form-control" type="text" placeholder="อำเภอ/เขต" maxlength="" name="" value="<?php echo $n->ampor2; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">จังหวัด
                                <input class="form-control" type="text" placeholder="จังหวัด" maxlength="" name="" value="<?php echo $n->provice2; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">รหัสไปรษณีย์
                                <input class="form-control" type="number" placeholder="รหัสไปรษณีย์" maxlength="" name="" value="<?php echo $n->zip; ?>" readonly />
                            </div>
                        </div>
                        <p style="max-width: 600px; font-size: 18px" align="left">หมายเลขโทรศัพท์ที่ทำงาน
                            <input class="form-control" type="number" placeholder="หมายเลขโทรศัพท์ที่ทำงาน" maxlength="" name="" value="<?php echo $n->phone_h; ?>" readonly />
                        </p>

                        <div class="dropdown-divider"></div>
                        <h5 align="left">ประวัติการศึกษา</h5>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">รหัสนักศึกษา
                                <input class="form-control" type="number" placeholder="รหัสนักศึกษา" maxlength="" name="" value="<?php echo $n->s_id; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">หมู่เรียน
                                <input class="form-control" type="text" placeholder="หมู่เรียน" maxlength="" name="" value="<?php echo $n->section; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-3" style="margin-bottom: 0">เกรดเฉลี่ย
                                <input class="form-control" type="number" placeholder="เกรดเฉลี่ย" maxlength="" name="" value="<?php echo $n->grade; ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row" style="max-width: 610px; font-size: 18px; margin-bottom: 16px" align="left">
                            <div class="form-group col-sm-6" style="margin-bottom: 0">ปีการศึกษาที่เข้าศึกษา
                                <input class="form-control" type="number" placeholder="ปีการศึกษาที่เข้าศึกษา" maxlength="" name="" value="<?php echo $n->regis_year; ?>" readonly />
                            </div>
                            <div class="form-group col-sm-6" style="margin-bottom: 0">ปีการศึกษาที่สำเร็จการศึกษา
                                <input class="form-control" type="number" placeholder="ปีการศึกษาที่สำเร็จการศึกษา" maxlength="" name="" value="<?php echo $n->sus_year; ?>" readonly />
                            </div>
                        </div>
                    <?php } ?>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo site_url('Welcome'); ?>"><button type="button" class="btn btn-danger">ปิด</button></a>
                </form>
            </div>

        </div>
    </div>
</div>